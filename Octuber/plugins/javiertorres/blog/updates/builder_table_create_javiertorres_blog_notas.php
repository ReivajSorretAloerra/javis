<?php namespace Javiertorres\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJaviertorresBlogNotas extends Migration
{
    public function up()
    {
        Schema::create('javiertorres_blog_notas', function($table)
        {
            $table->engine = 'InnoDB';
            $table->string('titulo');
            $table->text('descripcion')->nullable();
        });
    }
    
    
    public function down()
    {
        Schema::dropIfExists('javiertorres_blog_notas');
    }
}
