<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\xampp\htdocs\Octuber/themes/razas/pages/index.htm */
class __TwigTemplate_a1eab68bf7d9fd875860c9ebe239d41ce91f157cc9418bbea86d81bd043fc4e7 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!doctype html>
<html lang=\"en\">
  <head>
    <title>DogBeautifull</title>
    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">
    <script src=\"https://kit.fontawesome.com/d8d67c135a.js\" crossorigin=\"anonymous\"></script>
    <link href=\"https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800\" rel=\"stylesheet\">
    <script src=\"https://kit.fontawesome.com/d8d67c135a.js\"></script>
    <link href=\"";
        // line 10
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/animate.css");
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 11
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/bootstrap.css");
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 12
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/owl.carousel.min.css");
        echo "\" rel=\"stylesheet\">
    <!--Las hojas de estilos son pertenecientes al slider-->
    <link href=\"";
        // line 14
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/slider/css/estilos.css");
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 15
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/slider/css/font-awesome.css");
        echo "\" rel=\"stylesheet\">
    <!--Hasta aqui llegan las hojas de estilo del slider-->
    <!--Comiezan Hojas de estilos para el principal conjunto de imagenes de perros-->
    <link href=\"";
        // line 18
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/slider/css/conjunto_perros.css");
        echo "\" rel=\"stylesheet\">

    <!--Hasta aqui termina la seccion de hojas de estilo para la parte principal de las imagenes de los perros-->
    <link rel=\"stylesheet\" href=\"assets/fonts/ionicons/css/ionicons.min.css\">
    <link rel=\"stylesheet\" href=\"fonts/fontawesome/css/font-awesome.min.css\">

    <link rel=\"stylesheet\" href=\"fonts/flaticon/font/flaticon.css\">

    <!-- Theme Style -->
    
    <link href=\"";
        // line 28
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/style.css");
        echo "\" rel=\"stylesheet\">
  </head>
  <header role=\"banner\" id=\"header\">
    <nav class=\"navbar navbar-expand-lg navbar-dark bg-dark\">
      <div class=\"container\">
        <a class=\"navbar-brand absolute\" href=\"index.html\">breed</a>
        <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarsExample05\" aria-controls=\"navbarsExample05\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
          <span class=\"navbar-toggler-icon\"></span>
        </button>
  
        <div class=\"collapse navbar-collapse\" id=\"navbarsExample05\">
          <ul class=\"navbar-nav mx-auto pl-lg-5 pl-0\">
            <li class=\"nav-item\">
              <a class=\"nav-link active\" href=\"index.html\">Home</a>
            </li>
            <li class=\"nav-item\">
              <a class=\"nav-link\" href=\"about.html\">About</a>
            </li>
            <li class=\"nav-item dropdown\">
              <a class=\"nav-link dropdown-toggle\" href=\"services.html\" id=\"dropdown04\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">Breed</a>
              <div class=\"dropdown-menu\" aria-labelledby=\"dropdown04\">
                <a class=\"dropdown-item\" href=\"#\">German Shepherd</a>
                <a class=\"dropdown-item\" href=\"#\">Labrador</a>
                <a class=\"dropdown-item\" href=\"#\">Rottweiler</a>
              </div>
            </li>
            <li class=\"nav-item\">
              <a class=\"nav-link\" href=\"blog.html\">Blog</a>
            </li>
            <li class=\"nav-item\">
              <a class=\"nav-link\" href=\"contact.html\">Contact</a>
            </li>
          </ul>
          
        </div>
      </div>
    </nav>
  </header>
  <!-- END header -->
  <!--slider-->
  <body>


    
    
   <!--Carrusel de Imagenes-->

<div class=\"slideshow\">
  <ul class=\"slider\">

      <li>

  <img src=\"";
        // line 80
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/slider/img/slider-1.jpg");
        echo "\" alt=\"Image placeholder\" class=\"img-fluid\" />
          <section class=\"caption\">
              <h1 class=\"titulo_slider\">AMAMOS A LAS MASCOTAS.</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci quis ipsa, id quidem quisquam unde.</p>
    <br>
    <div class=\"btn-1\">
      <input type=\"submit\" value=\"Get Started\" class=\"btn\"><input type=\"submit\" value=\"Descargar\" class=\"btnn\">
    </div>
    
          </section>
      </li>
      <li>
  <img src=\"";
        // line 92
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/slider/img/slider-2.jpg");
        echo "\" alt=\"Image placeholder\" class=\"img-fluid\" />
          <section class=\"caption\">
              <h1 class=\"titulo_slider\">CUIDAR PERROS</h1>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci quis ipsa, id quidem quisquam unde.</p>
  </section>

</li>

  </ul>
  
  <ol class=\"pagination\">
      
  </ol>

  <div class=\"left\">
      <span class=\"fa fa-chevron-left\"></span>
  </div>

  <div class=\"right\">
      <span class=\"fa fa-chevron-right\"></span>
  </div>

</div>
<br>
<br>
<br>
<!-- Hasta aqui es el codigo del slider -->

    <!--Empieza la seccion principal de la imagen-->

    <section class=\"section element-animate\">
      <div class=\"container\">
        <div class=\"row justify-content-center mb-5\">
          <div class=\"col-md-4\"></div>
          <div class=\"col-md-8 section-heading\">
            <h2>It's a Dog Life</h2>
            <p class=\"small-sub-heading\">Curious story of Dogs</p>
          </div>
        </div>
        <div class=\"row\">
          <div class=\"col-md-4 mb-4\">
            
            <img src=\"";
        // line 134
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/dog_1.jpg");
        echo "\" alt=\"Image placeholder\" class=\"img-fluid\" />
          </div>
          <div class=\"col-md-4\">
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
            <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
            <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>
          </div>
          <div class=\"col-md-4\">
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
            <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
            <p><a href=\"#\">Learn More <span class=\"ion-ios-arrow-right\"></span></a></p>
          </div>
        </div>
      </div>
    </section>
   
    <section class=\"section bg-light\">
      <div class=\"container\">
        <div class=\"row justify-content-center mb-5 element-animate\">
          <div class=\"col-md-8 text-center\">
            <h2 class=\" heading mb-4\">Cada perro necesita una buena dueña</h2>
            <p class=\"mb-5 lead\">Tienes la oportunidad de convertirte en la persona mas deseada para alguno de estos animales.Porque no hacerlo??</p>
          </div>
        </div>
        <!--Carrusel de imagenes personas-->
        <div class=\"row element-animate\">
          <div class=\"major-caousel js-carousel-1 owl-carousel\">
            <div>
              <div class=\"media d-block media-custom text-center\">
                <a href=\"adoption-single.html\"><img src=\"";
        // line 163
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/slider/img/persona_1.jpg");
        echo "\" alt=\"Image placeholder\" class=\"img-fluid\" /></a>
                <div class=\"media-body\">
                  <h3 class=\"mt-0 text-black\">Mellisa Howard</h3>
                </div>
              </div>
            </div>
            <div>
              <div class=\"media d-block media-custom text-center\">
                <a href=\"adoption-single.html\"><img src=\"";
        // line 171
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/slider/img/person_2.jpg");
        echo "\" alt=\"Image placeholder\" class=\"img-fluid\" /></a>
                <div class=\"media-body\">
                  <h3 class=\"mt-0 text-black\">Mike Richardson</h3>
                </div>
              </div>
            </div>
            <div>
              <div class=\"media d-block media-custom text-center\">
                <a href=\"#\"><img src=\"";
        // line 179
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/person_3.jpg");
        echo "\" alt=\"Image placeholder\" class=\"img-fluid\" /></a>
                <div class=\"media-body\">
                  <h3 class=\"mt-0 text-black\">Charles White</h3>
                </div>
              </div>
            </div>
            <div>
              <div class=\"media d-block media-custom text-center\">
                <a href=\"adoption-single.html\"><img src=\"";
        // line 187
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/person_4.jpg");
        echo "\" alt=\"Image placeholder\" class=\"img-fluid\" /></a>
                <div class=\"media-body\">
                  <h3 class=\"mt-0 text-black\">Laura Smith</h3>
                </div>
              </div>
            </div>
          </div>
        </div>
         <!--Final de carrusel de imagenes-->
      </div>
    </section>
    <!-- END section -->

    <section class=\"section border-t\">
      <div class=\"container\">
        <div class=\"row justify-content-center mb-5 element-animate\">
          <div class=\"col-md-8 text-center\">
            <h2 class=\" heading mb-4\">Colecciones de razas de perros</h2>
            <p class=\"mb-5 lead\">Ya estas aqui , Porque marcharte sin hecharle un ojo  a la galeria de algunos de nuestros animales?.Encontraras de distintas razas y tamaños.Vamos a ello!!</p>
          </div>
        </div>
<!--Imagenes de los perros en conjunto-->
        <div class=\"row no-gutters\">
          <div class=\"col-md-4 element-animate\">
            <div class=\"c-img\">
              <a href=\"single.html\" class=\"link-thumbnail\">
                <div class=\"txt\">
                  <h3 id=\"h3\">German Shepherd</h3>
                </div>
                <span class=\"ion-plus icon\"></span>
                <img src=\"";
        // line 217
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/dog_1.jpg");
        echo "\" alt=\"Image placeholder\" id=\"img\"   class=\"img-fluid\" /> 
              </a>  
            </div>
             
            
          </div>
            <div class=\"col-md-4 element-animate\">
              <div class=\"c-img\">
                <a href=\"single.html\" class=\"link-thumbnail\">
                  <div class=\"txt\">
                    <h3 id=\"h3\">Labrador</h3>
                  </div>
                  <span class=\"ion-plus icon\"></span>
                  <img src=\"";
        // line 230
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/dog_2.jpg");
        echo "\" alt=\"Image placeholder\" class=\"img-fluid\" />
                </a>

              </div>
              
            </div>
          
          
            <div class=\"col-md-4 element-animate\">
              <div class=\"c-img\">
                <a href=\"single.html\" class=\"link-thumbnail\">
                  <div class=\"txt\">
                    <h3 id=\"h3\">Bulldog</h3>
                  </div>
                  <span class=\"ion-plus icon\"></span>
                  <img src=\"";
        // line 245
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/dog_3.jpg");
        echo "\" alt=\"Image placeholder\" class=\"img-fluid\" />
                </a>
              </div>
            </div>
          
         
          <div class=\"col-md-4 element-animate\">
            <div class=\"c-img\">
              <a href=\"single.html\" class=\"link-thumbnail\">
                <div class=\"txt\">
                  <h3 id=\"h3\">Rottweiler</h3>
                </div>
                <span class=\"ion-plus icon\"></span>
                <img src=\"";
        // line 258
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/dog_4.jpg");
        echo "\" alt=\"Image placeholder\" class=\"img-fluid\" />
              </a>
            </div>
            
          </div>
         
          
            <div class=\"col-md-4 element-animate\">
              <div class=\"c-img\">
                <a href=\"single.html\" class=\"link-thumbnail\">
                  <div class=\"txt\">
                    <h3 id=\"h3\">Beagle</h3>
                  </div>
                  <i class=\"far fa-plus\"></i>
                  <img src=\"";
        // line 272
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/dog_5.jpg");
        echo "\" alt=\"Image placeholder\" class=\"img-fluid\" />
                </a>
              </div>
             
            </div>
          
          
            <div class=\"col-md-4 element-animate\">
              <div class=\"c-img\" >
                <a href=\"single.html\" class=\"link-thumbnail\">
                  <div class=\"txt\">
                    <h3 id=\"h3\">Golden Retriever</h3>
                  </div>
                  <span class=\"ion-plus icon\"></span>
                  <img src=\"";
        // line 286
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/dog_6.jpg");
        echo "\" alt=\"Image placeholder\" class=\"img-fluid\" />
                </a>
              </div>
             
            </div>
          
        </div>
        
      </div>
    </section>
    <br>
    <br>
    <br>
    <br>
    <br> 
    <!-- END section -->

    <!--Comienza el blog-->

    <section class=\"section blog\">
      <div class=\"container\">

        <div class=\"row justify-content-center mb-5 element-animate\">
          <div class=\"col-md-8 text-center\">
            <h2 class=\" heading mb-4\">Recent Blog Post</h2>
            <p class=\"mb-5 lead\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi unde impedit, necessitatibus, soluta sit quam minima expedita atque corrupti reiciendis.</p>
          </div>
        </div>

        <div class=\"row\">
          <div class=\"col-md-6\">

            <div class=\"media mb-4 d-md-flex d-block element-animate\">
              <a href=\"#\" class=\"mr-5\"><img src=\"";
        // line 319
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/blog_1.jpg");
        echo "\" alt=\"Image placeholder\" class=\"img-fluid\" /></a>
              <div class=\"media-body\">
                <span class=\"post-meta\">Mar 26th, 2020</span>
                <h3 class=\"mt-2 text-black\"><a href=\"#\">Cómo entrenar a tu perro</a></h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam minus ipsa earum nemo consectetur cupiditate necessitatibus suscipit assumenda perspiciatis provident.</p>
                <p><a href=\"#\" class=\"btn btn-primary btn-sm\">Quieres saber mas??</a></p>
              </div>
            </div>



          </div>
          <div class=\"col-md-6\">
            <div class=\"media mb-4 d-md-flex d-block element-animate\">
              <a href=\"#\" class=\"mr-5\"><img src=\"";
        // line 333
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/blog_2.jpg");
        echo "\" alt=\"Image placeholder\" class=\"img-fluid\" /></a>
              <div class=\"media-body\">
                <span class=\"post-meta\">Mar 26th, 2020</span>
                <h3 class=\"mt-2 text-black\"><a href=\"#\">Encuentra la comida mas adecuada para tus perros</a></h3>
                <p><a href=\"#\" class=\"btn btn-primary btn-sm\">Quieres saber mas??</a></p>
              </div>
            </div>

            <div class=\"media mb-4 d-md-flex d-block element-animate\">
              <a href=\"#\" class=\"mr-5\"><img src=\"";
        // line 342
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/blog_3.jpg");
        echo "\" alt=\"Image placeholder\" class=\"img-fluid\" /></a>
              <div class=\"media-body\">
                <span class=\"post-meta\">Mar 26th, 2020</span>
                <h3 class=\"mt-2 text-black\"><a href=\"#\">Afectos del perro a la dueña</a></h3>
                <p><a href=\"#\" class=\"btn btn-primary btn-sm\">Quieres saber mas??</a></p>
              </div>
            </div>

          </div>
        </div>
      </div>
    </section>
    <!--Termina la seccion del blog-->
     
    <!--Footer-->

    ";
        // line 358
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("Footer"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 359
        echo "
    
    <!--Scripts-->
    <script src=\"";
        // line 362
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/main.js");
        echo "\"></script>
    <script src=\"";
        // line 363
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/owl.carousel.min.js");
        echo "\"></script>
    <script src=\"";
        // line 364
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/jsjquery.waypoints.min.js");
        echo "\"></script>
    <script src=\"";
        // line 365
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/popper.min.js");
        echo "\"></script>
    <script src=\"";
        // line 366
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/bootstrap.min.js");
        echo "\"></script>
    <script src=\"";
        // line 367
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/jquery-3.2.1.min.js");
        echo "\"></script>
<!--Hojas de javascript correspondientes al slider-->
<script src=\"";
        // line 369
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/slider/js/main.js");
        echo "\"></script>
<!--Final de las hojas de javascript-->

    

    ";
        // line 374
        echo $this->env->getExtension('Cms\Twig\Extension')->assetsFunction('js');
        echo $this->env->getExtension('Cms\Twig\Extension')->displayBlock('scripts');
        // line 375
        echo "  </body>
</html>";
    }

    public function getTemplateName()
    {
        return "C:\\xampp\\htdocs\\Octuber/themes/razas/pages/index.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  510 => 375,  507 => 374,  499 => 369,  494 => 367,  490 => 366,  486 => 365,  482 => 364,  478 => 363,  474 => 362,  469 => 359,  465 => 358,  446 => 342,  434 => 333,  417 => 319,  381 => 286,  364 => 272,  347 => 258,  331 => 245,  313 => 230,  297 => 217,  264 => 187,  253 => 179,  242 => 171,  231 => 163,  199 => 134,  154 => 92,  139 => 80,  84 => 28,  71 => 18,  65 => 15,  61 => 14,  56 => 12,  52 => 11,  48 => 10,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!doctype html>
<html lang=\"en\">
  <head>
    <title>DogBeautifull</title>
    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">
    <script src=\"https://kit.fontawesome.com/d8d67c135a.js\" crossorigin=\"anonymous\"></script>
    <link href=\"https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800\" rel=\"stylesheet\">
    <script src=\"https://kit.fontawesome.com/d8d67c135a.js\"></script>
    <link href=\"{{ 'assets/css/animate.css'|theme }}\" rel=\"stylesheet\">
    <link href=\"{{ 'assets/css/bootstrap.css'|theme }}\" rel=\"stylesheet\">
    <link href=\"{{ 'assets/css/owl.carousel.min.css'|theme }}\" rel=\"stylesheet\">
    <!--Las hojas de estilos son pertenecientes al slider-->
    <link href=\"{{ 'assets/slider/css/estilos.css'|theme }}\" rel=\"stylesheet\">
    <link href=\"{{ 'assets/slider/css/font-awesome.css'|theme }}\" rel=\"stylesheet\">
    <!--Hasta aqui llegan las hojas de estilo del slider-->
    <!--Comiezan Hojas de estilos para el principal conjunto de imagenes de perros-->
    <link href=\"{{ 'assets/slider/css/conjunto_perros.css'|theme }}\" rel=\"stylesheet\">

    <!--Hasta aqui termina la seccion de hojas de estilo para la parte principal de las imagenes de los perros-->
    <link rel=\"stylesheet\" href=\"assets/fonts/ionicons/css/ionicons.min.css\">
    <link rel=\"stylesheet\" href=\"fonts/fontawesome/css/font-awesome.min.css\">

    <link rel=\"stylesheet\" href=\"fonts/flaticon/font/flaticon.css\">

    <!-- Theme Style -->
    
    <link href=\"{{ 'assets/css/style.css'|theme }}\" rel=\"stylesheet\">
  </head>
  <header role=\"banner\" id=\"header\">
    <nav class=\"navbar navbar-expand-lg navbar-dark bg-dark\">
      <div class=\"container\">
        <a class=\"navbar-brand absolute\" href=\"index.html\">breed</a>
        <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarsExample05\" aria-controls=\"navbarsExample05\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
          <span class=\"navbar-toggler-icon\"></span>
        </button>
  
        <div class=\"collapse navbar-collapse\" id=\"navbarsExample05\">
          <ul class=\"navbar-nav mx-auto pl-lg-5 pl-0\">
            <li class=\"nav-item\">
              <a class=\"nav-link active\" href=\"index.html\">Home</a>
            </li>
            <li class=\"nav-item\">
              <a class=\"nav-link\" href=\"about.html\">About</a>
            </li>
            <li class=\"nav-item dropdown\">
              <a class=\"nav-link dropdown-toggle\" href=\"services.html\" id=\"dropdown04\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">Breed</a>
              <div class=\"dropdown-menu\" aria-labelledby=\"dropdown04\">
                <a class=\"dropdown-item\" href=\"#\">German Shepherd</a>
                <a class=\"dropdown-item\" href=\"#\">Labrador</a>
                <a class=\"dropdown-item\" href=\"#\">Rottweiler</a>
              </div>
            </li>
            <li class=\"nav-item\">
              <a class=\"nav-link\" href=\"blog.html\">Blog</a>
            </li>
            <li class=\"nav-item\">
              <a class=\"nav-link\" href=\"contact.html\">Contact</a>
            </li>
          </ul>
          
        </div>
      </div>
    </nav>
  </header>
  <!-- END header -->
  <!--slider-->
  <body>


    
    
   <!--Carrusel de Imagenes-->

<div class=\"slideshow\">
  <ul class=\"slider\">

      <li>

  <img src=\"{{ 'assets/slider/img/slider-1.jpg'|theme }}\" alt=\"Image placeholder\" class=\"img-fluid\" />
          <section class=\"caption\">
              <h1 class=\"titulo_slider\">AMAMOS A LAS MASCOTAS.</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci quis ipsa, id quidem quisquam unde.</p>
    <br>
    <div class=\"btn-1\">
      <input type=\"submit\" value=\"Get Started\" class=\"btn\"><input type=\"submit\" value=\"Descargar\" class=\"btnn\">
    </div>
    
          </section>
      </li>
      <li>
  <img src=\"{{ 'assets/slider/img/slider-2.jpg'|theme }}\" alt=\"Image placeholder\" class=\"img-fluid\" />
          <section class=\"caption\">
              <h1 class=\"titulo_slider\">CUIDAR PERROS</h1>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci quis ipsa, id quidem quisquam unde.</p>
  </section>

</li>

  </ul>
  
  <ol class=\"pagination\">
      
  </ol>

  <div class=\"left\">
      <span class=\"fa fa-chevron-left\"></span>
  </div>

  <div class=\"right\">
      <span class=\"fa fa-chevron-right\"></span>
  </div>

</div>
<br>
<br>
<br>
<!-- Hasta aqui es el codigo del slider -->

    <!--Empieza la seccion principal de la imagen-->

    <section class=\"section element-animate\">
      <div class=\"container\">
        <div class=\"row justify-content-center mb-5\">
          <div class=\"col-md-4\"></div>
          <div class=\"col-md-8 section-heading\">
            <h2>It's a Dog Life</h2>
            <p class=\"small-sub-heading\">Curious story of Dogs</p>
          </div>
        </div>
        <div class=\"row\">
          <div class=\"col-md-4 mb-4\">
            
            <img src=\"{{ 'assets/img/dog_1.jpg'|theme }}\" alt=\"Image placeholder\" class=\"img-fluid\" />
          </div>
          <div class=\"col-md-4\">
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
            <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
            <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>
          </div>
          <div class=\"col-md-4\">
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
            <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
            <p><a href=\"#\">Learn More <span class=\"ion-ios-arrow-right\"></span></a></p>
          </div>
        </div>
      </div>
    </section>
   
    <section class=\"section bg-light\">
      <div class=\"container\">
        <div class=\"row justify-content-center mb-5 element-animate\">
          <div class=\"col-md-8 text-center\">
            <h2 class=\" heading mb-4\">Cada perro necesita una buena dueña</h2>
            <p class=\"mb-5 lead\">Tienes la oportunidad de convertirte en la persona mas deseada para alguno de estos animales.Porque no hacerlo??</p>
          </div>
        </div>
        <!--Carrusel de imagenes personas-->
        <div class=\"row element-animate\">
          <div class=\"major-caousel js-carousel-1 owl-carousel\">
            <div>
              <div class=\"media d-block media-custom text-center\">
                <a href=\"adoption-single.html\"><img src=\"{{ 'assets/slider/img/persona_1.jpg'|theme }}\" alt=\"Image placeholder\" class=\"img-fluid\" /></a>
                <div class=\"media-body\">
                  <h3 class=\"mt-0 text-black\">Mellisa Howard</h3>
                </div>
              </div>
            </div>
            <div>
              <div class=\"media d-block media-custom text-center\">
                <a href=\"adoption-single.html\"><img src=\"{{ 'assets/slider/img/person_2.jpg'|theme }}\" alt=\"Image placeholder\" class=\"img-fluid\" /></a>
                <div class=\"media-body\">
                  <h3 class=\"mt-0 text-black\">Mike Richardson</h3>
                </div>
              </div>
            </div>
            <div>
              <div class=\"media d-block media-custom text-center\">
                <a href=\"#\"><img src=\"{{ 'assets/img/person_3.jpg'|theme }}\" alt=\"Image placeholder\" class=\"img-fluid\" /></a>
                <div class=\"media-body\">
                  <h3 class=\"mt-0 text-black\">Charles White</h3>
                </div>
              </div>
            </div>
            <div>
              <div class=\"media d-block media-custom text-center\">
                <a href=\"adoption-single.html\"><img src=\"{{ 'assets/img/person_4.jpg'|theme }}\" alt=\"Image placeholder\" class=\"img-fluid\" /></a>
                <div class=\"media-body\">
                  <h3 class=\"mt-0 text-black\">Laura Smith</h3>
                </div>
              </div>
            </div>
          </div>
        </div>
         <!--Final de carrusel de imagenes-->
      </div>
    </section>
    <!-- END section -->

    <section class=\"section border-t\">
      <div class=\"container\">
        <div class=\"row justify-content-center mb-5 element-animate\">
          <div class=\"col-md-8 text-center\">
            <h2 class=\" heading mb-4\">Colecciones de razas de perros</h2>
            <p class=\"mb-5 lead\">Ya estas aqui , Porque marcharte sin hecharle un ojo  a la galeria de algunos de nuestros animales?.Encontraras de distintas razas y tamaños.Vamos a ello!!</p>
          </div>
        </div>
<!--Imagenes de los perros en conjunto-->
        <div class=\"row no-gutters\">
          <div class=\"col-md-4 element-animate\">
            <div class=\"c-img\">
              <a href=\"single.html\" class=\"link-thumbnail\">
                <div class=\"txt\">
                  <h3 id=\"h3\">German Shepherd</h3>
                </div>
                <span class=\"ion-plus icon\"></span>
                <img src=\"{{ 'assets/img/dog_1.jpg'|theme }}\" alt=\"Image placeholder\" id=\"img\"   class=\"img-fluid\" /> 
              </a>  
            </div>
             
            
          </div>
            <div class=\"col-md-4 element-animate\">
              <div class=\"c-img\">
                <a href=\"single.html\" class=\"link-thumbnail\">
                  <div class=\"txt\">
                    <h3 id=\"h3\">Labrador</h3>
                  </div>
                  <span class=\"ion-plus icon\"></span>
                  <img src=\"{{ 'assets/img/dog_2.jpg'|theme }}\" alt=\"Image placeholder\" class=\"img-fluid\" />
                </a>

              </div>
              
            </div>
          
          
            <div class=\"col-md-4 element-animate\">
              <div class=\"c-img\">
                <a href=\"single.html\" class=\"link-thumbnail\">
                  <div class=\"txt\">
                    <h3 id=\"h3\">Bulldog</h3>
                  </div>
                  <span class=\"ion-plus icon\"></span>
                  <img src=\"{{ 'assets/img/dog_3.jpg'|theme }}\" alt=\"Image placeholder\" class=\"img-fluid\" />
                </a>
              </div>
            </div>
          
         
          <div class=\"col-md-4 element-animate\">
            <div class=\"c-img\">
              <a href=\"single.html\" class=\"link-thumbnail\">
                <div class=\"txt\">
                  <h3 id=\"h3\">Rottweiler</h3>
                </div>
                <span class=\"ion-plus icon\"></span>
                <img src=\"{{ 'assets/img/dog_4.jpg'|theme }}\" alt=\"Image placeholder\" class=\"img-fluid\" />
              </a>
            </div>
            
          </div>
         
          
            <div class=\"col-md-4 element-animate\">
              <div class=\"c-img\">
                <a href=\"single.html\" class=\"link-thumbnail\">
                  <div class=\"txt\">
                    <h3 id=\"h3\">Beagle</h3>
                  </div>
                  <i class=\"far fa-plus\"></i>
                  <img src=\"{{ 'assets/img/dog_5.jpg'|theme }}\" alt=\"Image placeholder\" class=\"img-fluid\" />
                </a>
              </div>
             
            </div>
          
          
            <div class=\"col-md-4 element-animate\">
              <div class=\"c-img\" >
                <a href=\"single.html\" class=\"link-thumbnail\">
                  <div class=\"txt\">
                    <h3 id=\"h3\">Golden Retriever</h3>
                  </div>
                  <span class=\"ion-plus icon\"></span>
                  <img src=\"{{ 'assets/img/dog_6.jpg'|theme }}\" alt=\"Image placeholder\" class=\"img-fluid\" />
                </a>
              </div>
             
            </div>
          
        </div>
        
      </div>
    </section>
    <br>
    <br>
    <br>
    <br>
    <br> 
    <!-- END section -->

    <!--Comienza el blog-->

    <section class=\"section blog\">
      <div class=\"container\">

        <div class=\"row justify-content-center mb-5 element-animate\">
          <div class=\"col-md-8 text-center\">
            <h2 class=\" heading mb-4\">Recent Blog Post</h2>
            <p class=\"mb-5 lead\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi unde impedit, necessitatibus, soluta sit quam minima expedita atque corrupti reiciendis.</p>
          </div>
        </div>

        <div class=\"row\">
          <div class=\"col-md-6\">

            <div class=\"media mb-4 d-md-flex d-block element-animate\">
              <a href=\"#\" class=\"mr-5\"><img src=\"{{ 'assets/img/blog_1.jpg'|theme }}\" alt=\"Image placeholder\" class=\"img-fluid\" /></a>
              <div class=\"media-body\">
                <span class=\"post-meta\">Mar 26th, 2020</span>
                <h3 class=\"mt-2 text-black\"><a href=\"#\">Cómo entrenar a tu perro</a></h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam minus ipsa earum nemo consectetur cupiditate necessitatibus suscipit assumenda perspiciatis provident.</p>
                <p><a href=\"#\" class=\"btn btn-primary btn-sm\">Quieres saber mas??</a></p>
              </div>
            </div>



          </div>
          <div class=\"col-md-6\">
            <div class=\"media mb-4 d-md-flex d-block element-animate\">
              <a href=\"#\" class=\"mr-5\"><img src=\"{{ 'assets/img/blog_2.jpg'|theme }}\" alt=\"Image placeholder\" class=\"img-fluid\" /></a>
              <div class=\"media-body\">
                <span class=\"post-meta\">Mar 26th, 2020</span>
                <h3 class=\"mt-2 text-black\"><a href=\"#\">Encuentra la comida mas adecuada para tus perros</a></h3>
                <p><a href=\"#\" class=\"btn btn-primary btn-sm\">Quieres saber mas??</a></p>
              </div>
            </div>

            <div class=\"media mb-4 d-md-flex d-block element-animate\">
              <a href=\"#\" class=\"mr-5\"><img src=\"{{ 'assets/img/blog_3.jpg'|theme }}\" alt=\"Image placeholder\" class=\"img-fluid\" /></a>
              <div class=\"media-body\">
                <span class=\"post-meta\">Mar 26th, 2020</span>
                <h3 class=\"mt-2 text-black\"><a href=\"#\">Afectos del perro a la dueña</a></h3>
                <p><a href=\"#\" class=\"btn btn-primary btn-sm\">Quieres saber mas??</a></p>
              </div>
            </div>

          </div>
        </div>
      </div>
    </section>
    <!--Termina la seccion del blog-->
     
    <!--Footer-->

    {% partial 'Footer' %}

    
    <!--Scripts-->
    <script src=\"{{ 'assets/js/main.js'|theme }}\"></script>
    <script src=\"{{ 'assets/js/owl.carousel.min.js'|theme }}\"></script>
    <script src=\"{{ 'assets/jsjquery.waypoints.min.js'|theme }}\"></script>
    <script src=\"{{ 'assets/js/popper.min.js'|theme }}\"></script>
    <script src=\"{{ 'assets/js/bootstrap.min.js'|theme }}\"></script>
    <script src=\"{{ 'assets/js/jquery-3.2.1.min.js'|theme }}\"></script>
<!--Hojas de javascript correspondientes al slider-->
<script src=\"{{ 'assets/slider/js/main.js'|theme }}\"></script>
<!--Final de las hojas de javascript-->

    

    {%scripts%}
  </body>
</html>", "C:\\xampp\\htdocs\\Octuber/themes/razas/pages/index.htm", "");
    }
}
